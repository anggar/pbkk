<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Prices extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("product_model");
        $this->load->model("price_model");
        $this->load->library('form_validation');
        $this->load->model("user_model");
		if($this->user_model->isNotLogin()) redirect(site_url('admin/login'));
    }

    public function index()
    {
        $data["products"] = $this->product_model->getAll();
        $data["prices"] = $this->price_model->getAll();
        $data["products_id"] = $this->product_model->getAllId();

        $this->load->view("admin/price/list", $data);
    }

    public function add()
    {
        $price = $this->price_model;
        $validation = $this->form_validation;
        $validation->set_rules($price->rules());

        if ($validation->run()) {
            $price->save();
            // $product->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["products"] = $this->product_model->getAll();

        $this->load->view("admin/price/new_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/prices');
       

        $price = $this->price_model;
        $validation = $this->form_validation;
        $validation->set_rules($price->rules());

        if ($validation->run()) {
            $price->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["products"] = $this->product_model->getAll();
        $data["price"] = $price->getById($id); 
        
        if (!$data["price"]) show_404();
        
        $this->load->view("admin/price/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->price_model->delete($id)) {
            redirect(site_url('admin/prices'));
        }
    }
}
