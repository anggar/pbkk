<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Overview extends CI_Controller {
    public function __construct()
    {
		parent::__construct();
		$this->load->model("stat_model_helper");
		$this->load->model("user_model");
		if($this->user_model->isNotLogin()) {
			redirect(site_url('admin/login'));
		}
	}

	public function index()
	{
		// load view admin/overview.php
		$entities = ["products", "prices", "transactions"];
		$data = array();

		foreach ($entities as $entity) {
			$data[$entity] = $this->stat_model_helper->getCount($entity);
		}

        $this->load->view("admin/overview", $data);
	}
}
