<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("transaction_model");
        $this->load->model("product_model");
        $this->load->model("price_model");
        $this->load->library('form_validation');
        $this->load->model("user_model");
		if($this->user_model->isNotLogin()) redirect(site_url('admin/login'));
    }

    public function index()
    {
        $data["transactions"] = $this->transaction_model->getAll();

        $this->load->view("admin/transaction/list", $data);
    }

    public function add()
    {
        $transaction = $this->transaction_model;
        $price = $this->price_model;
        $product = $this->product_model;
        $validation = $this->form_validation;
        $validation->set_rules($transaction->rules());

        if ($validation->run()) {
            $transaction->save();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["prices"] = $price->getAll();
        $data["products"] = $product->getAll();

        $this->load->view("admin/transaction/new_form", $data);
    }

    public function edit($id = null)
    {
        if (!isset($id)) redirect('admin/transactions');
       
        $transaction = $this->transaction_model;
        $validation = $this->form_validation;
        $validation->set_rules($transaction->rules());

        if ($validation->run()) {
            $transaction->update();
            $this->session->set_flashdata('success', 'Berhasil disimpan');
        }

        $data["transaction"] = $transaction->getById($id);
        $data["prices"] = $this->price_model->getBytransactionIdOrNull($transaction->price_id);

        if (!$data["transaction"]) show_404();
        
        $this->load->view("admin/transaction/edit_form", $data);
    }

    public function delete($id=null)
    {
        if (!isset($id)) show_404();
        
        if ($this->transaction_model->delete($id)) {
            redirect(site_url('admin/transactions'));
        }
    }
}
