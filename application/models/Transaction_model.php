<?php

class Transaction_model extends CI_Model {
    private $_table = "transactions";

    public $trx_id;
    // public $invoice_id;
    public $product_id;
    public $price_id;
    public $count;
    public $date_created;

    private function _fillable() {
        return [
            'price_id' => $this->price_id, 
            'product_id' => $this->product_id, 
            'count' => $this->count, 
        ];
    }

    public function rules()
    {
        return [
            ['field' => 'product_id',
            'label' => 'Product',
            'rules' => 'required'],

            ['field' => 'price_id',
            'label' => 'Price',
            'rules' => 'required'],
            
            ['field' => 'count',
            'label' => 'Count',
            'rules' => 'required'],
        ];
    }

    public function getAll()
    {
        return $this->db->select('products.id as product_id, products.name, prices.price, transactions.*')->from($this->_table)
                        ->join('prices', $this->_table.'.price_id = prices.id')
                        ->join('products', $this->_table.'.product_id = products.id')
                        ->get()->result();
    }

    public function getById(Int $id)
    {
        return $this->db->select('*, products.id as product_id', 'prices.id as price_id')->from($this->_table)
                        ->join('prices', $this->_table.'.price_id = prices.id')
                        ->join('products', $this->_table.'product_id = products.id')
                        ->where(["transactions.id" => $id])
                        ->get()->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->price_id = $post["price_id"];
        $this->product_id = $post["product_id"];
        $this->count = $post["count"];

        $this->db->insert($this->_table, $this->_fillable());
    }

    public function delete(int $id) {
        return $this->db->delete($this->_table, array("id" => $id));
    }
}

?>