<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model
{
    private $_table = "products";

    public $id;
    public $price_id;
    public $price;
    public $currency;
    public $name;
    public $image = "default.jpg";
    public $description;

    private function _fillable() {
        return [
            'price_id' => $this->price_id, 
            'name' => $this->name, 
            'image' => $this->image, 
            'description' => $this->description];
    }

    public function rules()
    {
        return [
            ['field' => 'name',
            'label' => 'Name',
            'rules' => 'required'],

            ['field' => 'price_id',
            'label' => 'Price',
            'rules' => 'numeric'],
            
            ['field' => 'description',
            'label' => 'Description',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->select('products.*, prices.price')->from($this->_table)
                        ->join('prices', 'products.price_id = prices.id')
                        ->get()->result();
    }

    public function getAllId()
    {
        return $this->db->select("id")->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->select('*, products.id', 'prices.id as price_id')->from($this->_table)
                        ->join('prices', 'products.price_id = prices.id')
                        ->where(["products.id" => $id])
                        ->get()->row();
    }

    public function save()
    {
        $post = $this->input->post();

        $this->name = $post["name"];
		$this->price_id = $post["price_id"];
		$this->image = $this->_uploadImage();
        $this->description = $post["description"];
        $this->db->insert($this->_table, $this->_fillable());
    }

    public function update()
    {
        $post = $this->input->post();
        $this->id = $post["id"];
        $this->name = $post["name"];
		$this->price_id = $post["price_id"];
		
		
		if (!empty($_FILES["image"]["name"])) {
            $this->image = $this->_uploadImage();
        } else {
            $this->image = $post["old_image"];
		}

        $this->description = $post["description"];
        $this->db->update($this->_table, $this->_fillable(), array('id' => $post['id']));
    }

    public function delete($id)
    {
		$this->_deleteImage($id);
        return $this->db->delete($this->_table, array("id" => $id));
	}
	
	private function _uploadImage()
	{
		$config['upload_path']          = './upload/product/';
		$config['allowed_types']        = 'gif|jpg|png';
		$config['file_name']            = $this->id;
		$config['overwrite']			= true;
		$config['max_size']             = 1024; // 1MB
		// $config['max_width']            = 1024;
		// $config['max_height']           = 768;

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {
			return $this->upload->data("file_name");
		}
		
		return "default.jpg";
	}

	private function _deleteImage($id)
	{
		$product = $this->getById($id);
		if ($product->image != "default.jpg") {
			$filename = explode(".", $product->image)[0];
			return array_map('unlink', glob(FCPATH."upload/product/$filename.*"));
		}
	}

}
