
<?php

class Stat_model_helper extends CI_Model {
    public $count = array();

    public function getCount(string $table)
    {
        return $this->db->select('count(*) as count')->from($table)
                        ->get()->row();
    }
}

?>