<?php

class Invoice_model extends CI_Model {
    private $_table = "invoices";

    public $invoice_id;
    public $date_created;

    public function rules()
    {
        return [
            ['field' => 'price',
            'label' => 'Price',
            'rules' => 'required'],

            ['field' => 'product_id',
            'label' => 'Price',
            'rules' => 'required']
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    
    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["price_id" => $id])->row();
    }

    public function save()
    {
        $post = $this->input->post();
        $this->price = $post["price"];
        $this->product_id = $post["product_id"];
        $this->currency = $post["currency"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $put = $this->input->put();
        $this->price = $put["price"];
        $this->currency = $put["currency"];
        $this->db->update($this->_table, $this, array('price_id' => $put['id']));
    }

}

?>