<?php

class Price_model extends CI_Model {
    private $_table = "prices";

    public $id;
    public $product_id;
    public $price;
    public $currency;

    public function rules()
    {
        return [
            ['field' => 'price',
            'label' => 'Price',
            'rules' => 'required'],
            ['field' => 'price',
            'label' => 'Price',
            'rules' => 'numeric'],
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }

    public function getById($id)
    {
        return $this->db->get_where($this->_table, ["id" => $id])->row();
    }

    public function getByProductId($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id])->result();
    }

    public function getByProductIdOrNull($id)
    {
        return $this->db->get_where($this->_table, ["product_id" => $id, "product_id" => null])->result();
    }

    public function save()
    {
        $post = $this->input->post();       
        
        $this->price = $post["price"];
        $this->product_id = $post["product_id"];
        $this->currency = $post["currency"];
        $this->db->insert($this->_table, $this);
    }

    public function update()
    {
        $put = $this->input->post();
        $this->price = $put["price"];
        $this->product_id = $put["product_id"];
        $this->currency = $put["currency"];
        $this->db->update($this->_table, $this, array('id' => $put['id']));
    }

    public function delete($id)
    {
        return $this->db->delete($this->_table, array("id" => $id));
	}
}

?>