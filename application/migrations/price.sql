-- up

CREATE TABLE `prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11),
  `currency` enum('idr','jpy', 'usd') NOT NULL DEFAULT 'idr',
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
-- down

DROP TABLE `prices`;