-- up

CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT 'default.jpg',
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `products` 
  ADD CONSTRAINT PRODUCT_PRICE_FK
  FOREIGN KEY (`price_id`)
  REFERENCES prices(`id`);

-- down

DROP TABLE `products`;