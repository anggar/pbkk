-- up

CREATE TABLE `invoices` (
  `id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- down

DROP TABLE `invoices`;