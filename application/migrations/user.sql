-- up

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `role` enum('admin','customer') NOT NULL DEFAULT 'customer',
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `photo` varchar(64) NOT NULL DEFAULT 'user_no_image.jpg',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

INSERT INTO `users` (`username`, `password`, `email`, `full_name`, `phone`, `role`) VALUES ('admin', '$2y$10$43WRLWd/Ux1TecpEtCsXaunI9EB5LI41KFNFDQNX6F2S1INV.D7xu
', 'admin@adm.in', 'Admin', 
'089612342468', 'admin');

-- down

DROP TABLE `users`;