-- up

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  -- `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT 1,
  `date_created` datetime NOT NULL DEFAULT NOW(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE `transactions` ADD CONSTRAINT
  FOREIGN KEY (`price_id`)
  REFERENCES prices(`id`);

ALTER TABLE `transactions` ADD CONSTRAINT
  FOREIGN KEY (`product_id`)
  REFERENCES products(`id`);

-- ALTER TABLE `products` ADD CONSTRAINT
--   FOREIGN KEY (`price_id`)
--   REFERENCES prices(`id`);

-- down

DROP TABLE `transactions`;